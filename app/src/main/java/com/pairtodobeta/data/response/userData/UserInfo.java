
package com.pairtodobeta.data.response.userData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class UserInfo extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("nic_name")
    @Expose
    private String nicName;
    @SerializedName("registred")
    @Expose
    private String registred;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("tel")
    @Expose
    private String tel;
    @SerializedName("back_email")
    @Expose
    private String backEmail;
    @SerializedName("pair_id")
    @Expose
    private String pairId;
    @SerializedName("pair_name")
    @Expose
    private String pairName;
    @SerializedName("vk_id")
    @Expose
    private String vkId;
    @SerializedName("vk_name")
    @Expose
    private String vkName;
    @SerializedName("vk_photo")
    @Expose
    private String vkPhoto;
    @SerializedName("fb_id")
    @Expose
    private String fbId;
    @SerializedName("fb_name")
    @Expose
    private String fbName;
    @SerializedName("fb_photo")
    @Expose
    private String fbPhoto;
    @SerializedName("tw_id")
    @Expose
    private String twId;
    @SerializedName("tw_name")
    @Expose
    private String twName;
    @SerializedName("tw_photo")
    @Expose
    private String twPhoto;
    @SerializedName("last_modify")
    @Expose
    private String lastModify;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("photo_real")
    @Expose
    private String photoReal;
    @SerializedName("bg_thema")
    @Expose
    private String bgThema;
    @SerializedName("color1")
    @Expose
    private String color1;
    @SerializedName("color2")
    @Expose
    private String color2;
    @SerializedName("is_vip")
    @Expose
    private String isVip;
    @SerializedName("vip_date")
    @Expose
    private String vipDate;
    @SerializedName("pair_photo")
    @Expose
    private String pairPhoto;
    @SerializedName("is_email")
    @Expose
    private String isEmail;
    @SerializedName("is_vk")
    @Expose
    private String isVk;
    @SerializedName("is_fb")
    @Expose
    private String isFb;
    @SerializedName("is_tw")
    @Expose
    private String isTw;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNicName() {
        return nicName;
    }

    public void setNicName(String nicName) {
        this.nicName = nicName;
    }

    public String getRegistred() {
        return registred;
    }

    public void setRegistred(String registred) {
        this.registred = registred;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getBackEmail() {
        return backEmail;
    }

    public void setBackEmail(String backEmail) {
        this.backEmail = backEmail;
    }

    public String getPairId() {
        return pairId;
    }

    public void setPairId(String pairId) {
        this.pairId = pairId;
    }

    public String getPairName() {
        return pairName;
    }

    public void setPairName(String pairName) {
        this.pairName = pairName;
    }

    public String getVkId() {
        return vkId;
    }

    public void setVkId(String vkId) {
        this.vkId = vkId;
    }

    public String getVkName() {
        return vkName;
    }

    public void setVkName(String vkName) {
        this.vkName = vkName;
    }

    public String getVkPhoto() {
        return vkPhoto;
    }

    public void setVkPhoto(String vkPhoto) {
        this.vkPhoto = vkPhoto;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public String getFbName() {
        return fbName;
    }

    public void setFbName(String fbName) {
        this.fbName = fbName;
    }

    public String getFbPhoto() {
        return fbPhoto;
    }

    public void setFbPhoto(String fbPhoto) {
        this.fbPhoto = fbPhoto;
    }

    public String getTwId() {
        return twId;
    }

    public void setTwId(String twId) {
        this.twId = twId;
    }

    public String getTwName() {
        return twName;
    }

    public void setTwName(String twName) {
        this.twName = twName;
    }

    public String getTwPhoto() {
        return twPhoto;
    }

    public void setTwPhoto(String twPhoto) {
        this.twPhoto = twPhoto;
    }

    public String getLastModify() {
        return lastModify;
    }

    public void setLastModify(String lastModify) {
        this.lastModify = lastModify;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getPhotoReal() {
        return photoReal;
    }

    public void setPhotoReal(String photoReal) {
        this.photoReal = photoReal;
    }

    public String getBgThema() {
        return bgThema;
    }

    public void setBgThema(String bgThema) {
        this.bgThema = bgThema;
    }

    public String getColor1() {
        return color1;
    }

    public void setColor1(String color1) {
        this.color1 = color1;
    }

    public String getColor2() {
        return color2;
    }

    public void setColor2(String color2) {
        this.color2 = color2;
    }

    public String getIsVip() {
        return isVip;
    }

    public void setIsVip(String isVip) {
        this.isVip = isVip;
    }

    public String getVipDate() {
        return vipDate;
    }

    public void setVipDate(String vipDate) {
        this.vipDate = vipDate;
    }

    public String getPairPhoto() {
        return pairPhoto;
    }

    public void setPairPhoto(String pairPhoto) {
        this.pairPhoto = pairPhoto;
    }

    public String getIsEmail() {
        return isEmail;
    }

    public void setIsEmail(String isEmail) {
        this.isEmail = isEmail;
    }

    public String getIsVk() {
        return isVk;
    }

    public void setIsVk(String isVk) {
        this.isVk = isVk;
    }

    public String getIsFb() {
        return isFb;
    }

    public void setIsFb(String isFb) {
        this.isFb = isFb;
    }

    public String getIsTw() {
        return isTw;
    }

    public void setIsTw(String isTw) {
        this.isTw = isTw;
    }

}
