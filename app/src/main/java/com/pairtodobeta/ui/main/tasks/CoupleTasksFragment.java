package com.pairtodobeta.ui.main.tasks;

import android.view.View;

import com.pairtodobeta.data.DataService;
import com.pairtodobeta.data.response.tasks.GetTasksResponse;
import com.pairtodobeta.data.settings.SettingsRepository;
import com.pairtodobeta.ui.BaseActivity;
import com.pairtodobeta.utils.PrefRepository;

public class CoupleTasksFragment extends BaseTasksFragment {
    public CoupleTasksFragment() {
    }

    public static CoupleTasksFragment newInstance() {
        return new CoupleTasksFragment();
    }

    @Override
    protected void getTasks() {
        mSwipeRefreshLayout.setRefreshing(true);
        if (isNetworkAvailable()) {
            DataService.init().getMyTasks(new DataService.onGetMyTasks() {
                @Override
                public void onGetMyTasksResult(GetTasksResponse response) {
                    if (isAdded()) {
                        CoupleTasksAdapter adapter = new CoupleTasksAdapter(getActivity(), response.getResult(),
                                SettingsRepository.getUserInfo(BaseActivity.realm).getPairId());
                        mRecyclerView.setAdapter(adapter);
                        mSwipeRefreshLayout.setRefreshing(false);

                        if (response.getResult().size() == 0)
                            noTasksView.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onGetMyTasksError() {

                }
            }, PrefRepository.getToken(getActivity()));
        }
        else {
            setNoInternetView();
        }
    }

}
