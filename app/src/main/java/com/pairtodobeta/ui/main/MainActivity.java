package com.pairtodobeta.ui.main;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.pairtodobeta.PairTodoApplication;
import com.pairtodobeta.R;
import com.pairtodobeta.data.DataService;
import com.pairtodobeta.data.response.userPhoto.UserImage;
import com.pairtodobeta.router.FragmentRouter;
import com.pairtodobeta.ui.BaseActivity;
import com.pairtodobeta.ui.main.profile.ProfileFragment;
import com.pairtodobeta.ui.main.shop.ShopFragment;
import com.pairtodobeta.ui.main.tasks.TasksFragment;
import com.pairtodobeta.utils.AnimationBuilderHelper;
import com.pairtodobeta.utils.Constants;
import com.pairtodobeta.utils.PrefRepository;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.io.File;
import java.io.IOException;

import butterknife.Bind;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

import static com.pairtodobeta.utils.FileUtil.reduceFile;

public class MainActivity extends BaseActivity {
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.bottomBar)
    BottomBar mBottomBar;
    @Bind(R.id.paradelImageToolbar)
    ImageView pairToDoImage;
    @Bind(R.id.mainFrame)
    ViewGroup mainFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PairTodoApplication.checkPulseInBackground(getApplicationContext(),
                PrefRepository.getToken(getApplicationContext()));


        mBottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {

                if (tabId == R.id.tab_favorites) {
                    ViewCompat.setElevation(mainFrame, convertDpToPixel(0, MainActivity.this));
                    FragmentRouter.showMainTabFragment(MainActivity.this, ProfileFragment.newInstance());
                }

                if (tabId == R.id.tab_tasks) {
                    ViewCompat.setElevation(mainFrame, convertDpToPixel(4, MainActivity.this));
                    FragmentRouter.showMainTabFragment(MainActivity.this, TasksFragment.newInstance());
                }

                if (tabId == R.id.tab_shop) {
                    ViewCompat.setElevation(mainFrame, convertDpToPixel(0, MainActivity.this));
                    FragmentRouter.showMainTabFragment(MainActivity.this, ShopFragment.newInstance());
                }
            }
        });

        AnimationBuilderHelper.startIntroToolbarAnimation(this, pairToDoImage);
        AnimationBuilderHelper.startIntroBottomAnimation(this, mBottomBar);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Constants.CHANGE_TASK:
                    FragmentRouter.showMainTabFragment(MainActivity.this, TasksFragment.newInstance());
                    break;
            }

            initGalleryCallback(requestCode, resultCode, data);

        } else {
        }
    }

    public void initGalleryCallback(int requestCode, int resultCode, Intent data){

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(MainActivity.this, "Ошибка загрузки! Попробуйте еще раз!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {

                try {
                    DataService.init().uploadUserImage(reduceFile(imageFile), new DataService.onUploadUserPhoto() {
                        @Override
                        public void onUploadResult(UserImage image) {
                            FragmentRouter.showMainTabFragment(MainActivity.this, ProfileFragment.newInstance());
                        }

                        @Override
                        public void onUploadError(String error) {
                            FragmentRouter.showMainTabFragment(MainActivity.this, ProfileFragment.newInstance());
                        }
                }, PrefRepository.getToken(MainActivity.this));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(MainActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

}
