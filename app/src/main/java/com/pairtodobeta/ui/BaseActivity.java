package com.pairtodobeta.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import com.pairtodobeta.R;
import com.pairtodobeta.db.PairToDoCacheDao;
import com.pairtodobeta.utils.PrefRepository;
import com.pairtodobeta.utils.Themes;

import java.io.File;

import butterknife.ButterKnife;
import io.realm.Realm;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity {

    public static Realm realm;
    protected PairToDoCacheDao cacheManager = new PairToDoCacheDao();

    public static boolean deleteFile(File file) {

        boolean deletedAll = true;

        if (file != null) {

            if (file.isDirectory()) {

                String[] children = file.list();

                for (int i = 0; i < children.length; i++) {

                    deletedAll = deleteFile(new File(file, children[i])) && deletedAll;

                }

            } else {

                deletedAll = file.delete();

            }

        }

        return deletedAll;

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        chooseUserTheme();
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        realm = Realm.getDefaultInstance();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityMgr.getActiveNetworkInfo();
        /// if no network is available networkInfo will be null
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    private void chooseUserTheme() {

        switch (PrefRepository.getTheme(this)) {
            case Themes.THEME_DEFAULT:
                setTheme(R.style.Theme_Pairtodo_Default);
                break;
            case Themes.THEME_PARIS:
                setTheme(R.style.Theme_Pairtodo_Paris);
                break;
            case Themes.THEME_SEA:
                setTheme(R.style.Theme_Pairtodo_Sea);
                break;
            case Themes.THEME_CAT:
                setTheme(R.style.Theme_Pairtodo_Cat);
                break;
            case Themes.THEME_ROAD:
                setTheme(R.style.Theme_Pairtodo_Road);
                break;
            case Themes.THEME_WITCH:
                setTheme(R.style.Theme_Pairtodo_Witch);
                break;
            case Themes.THEME_CUTE_PURPLE:
                setTheme(R.style.Theme_Pairtodo_CutePurple);
                break;
            case Themes.THEME_PURPLE_HAZE:
                setTheme(R.style.Theme_Pairtodo_PurpleHaze);
                break;
            case Themes.THEME_SUMMER:
                setTheme(R.style.Theme_Pairtodo_Summer);
                break;
            case Themes.THEME_BEAUTY:
                setTheme(R.style.Theme_Pairtodo_Beauty);
                break;
            case "":
                setTheme(R.style.Theme_Pairtodo_Default);
                break;
            default: setTheme(R.style.Theme_Pairtodo_Default);

        }
    }

    public void clearApplicationData() {

        SharedPreferences settings = getSharedPreferences("settings", 0);
        settings.edit().clear().commit();
        File cacheDirectory = getCacheDir();
        File applicationDirectory = new File(cacheDirectory.getParent());
        if (applicationDirectory.exists()) {

            String[] fileNames = applicationDirectory.list();

            for (String fileName : fileNames) {

                if (!fileName.equals("lib")) {

                    deleteFile(new File(applicationDirectory, fileName));

                }

            }

        }
        realm.beginTransaction();
        eraseRealm();
        realm.commitTransaction();
    }

    public void eraseRealm() {
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
    }


}
